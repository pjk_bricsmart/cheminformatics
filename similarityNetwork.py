import pandas as pd
import numpy as np
import math
import joblib
import csv
import datetime

from pandas import DataFrame
from rdkit import Chem
from rdkit import DataStructs
from rdkit.Chem import PandasTools
from rdkit.Chem import Descriptors
from rdkit.Chem import inchi
from rdkit.Chem import Draw
from rdkit.ML.Descriptors import MoleculeDescriptors

import networkx as nx

df = pd.read_csv('data/activity_classes_ChEMBL24.dat', sep='\t')

TargetNameCounts = df.TargetName.value_counts()

Alpha1a = df[df.TargetName.eq('Alpha-1a adrenergic receptor')]

# similarity matrix
sm = pd.DataFrame(np.zeros(shape=(len(Alpha1a), len(Alpha1a))))

for index, row in Alpha1a.iterrows():
    Alpha1a.loc[index, 'ROMol'] = Chem.MolFromSmiles(row['SMILES'])
    
fps = []
for index, row in Alpha1a.iterrows():
    fps.append(Chem.RDKFingerprint(row['ROMol'], maxPath = 5))

for i in range(len(Alpha1a)):
    for j in range(len(Alpha1a)):
        sm[i][j] = DataStructs.TanimotoSimilarity(fps[i], fps[j])

# adjacency matrix
am = pd.DataFrame(np.zeros(shape=(len(Alpha1a), len(Alpha1a))))

# adjacent == Tanimoto similarity >= threshold
threshold = 0.75
for i in range(len(Alpha1a)):
    for j in range(len(Alpha1a)):
        if sm[i][j] >= threshold:
            am[i][j] = 1
        else:
            am[i][j] = 0
            
G = nx.from_pandas_adjacency(am)
print(nx.info(G))
nx.draw(G, pos=nx.spring_layout(G), node_size = 50, node_color = "#009fe3",
        alpha = 0.5)
nx.draw(G, pos=nx.spring_layout(G), node_size = 50, node_color = "#009fe3",
        alpha = 0.5, with_labels = True)
