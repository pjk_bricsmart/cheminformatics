# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import PandasTools
import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt 
import numpy as np
import math
from sklearn.ensemble import RandomForestRegressor

import molvs
from molvs import Standardizer
from molvs.normalize import Normalization
from molvs import standardize_smiles
norms = (
    Normalization('Nitro to N+(O-)=O', '[*:1][N,P,As,Sb:2](=[O,S,Se,Te:3])=[O,S,Se,Te:4]>>[*:1][*+1:2]([*-1:3])=[*:4]'),
    Normalization('Pyridine oxide to n+O-', '[n:1]=[O:2]>>[n+:1][O-:2]'),
)

mol = Chem.MolFromSmiles('[Na]OC(=O)c1ccc(C[S+2]([O-])([O-]))cc1')
mol

standardize_smiles('[Na]OC(=O)c1ccc(C[S+2]([O-])([O-]))cc1')
standardize_smiles('[N](=O)(O)c1cccc(C)c1')

s = Standardizer(normalizations=norms)
# s = Standardizer()
smol = s.standardize(mol)
smol

train = pd.read_csv("data/TrainingSet.csv")
test = pd.read_csv("data/TestSet.csv")
external = pd.read_csv("data/ExternalSet.csv")

mols = []
for i in range(len(train)):
    try:
        m = Chem.MolFromSmiles(train.iloc[i,1])
        mols.append((m, train.iloc[i]))
    except:
        print("bad record")


try:
  print(x)
except:
  print("An exception occurred")
