import bioalerts
import numpy as np


# bioalerts: LoadMolecules

from numpy import savetxt as _savetxt
from rdkit.Chem import SmilesMolSupplier as _SmilesMolSupplier, MolFromMol2Block as _MolFromMol2Block, SDMolSupplier as _SDMolSupplier
from os.path import splitext as _splitext, exists as _exists 
from operator import add as _add
from rdkit.Chem.AllChem import GetMorganFingerprintAsBitVect as  _GetMorganFingerprintAsBitVect, GetMorganFingerprint as _GetMorganFingerprint



def RetrieveMol2Block(fileLikeObject, delimiter="@<TRIPOS>MOLECULE"):
    """Generator to retrieve one mol2 block at a time when parsing a mol file
    """
    mol2 = []
    for line in fileLikeObject:
        if line.startswith(delimiter) and mol2:
            yield "".join(mol2)
            mol2 = []
        mol2.append(line)
    if mol2:
        yield "".join(mol2)


class LoadMolecules:    
    """Load molecules from (i) smiles, (ii) sdf, and (iii) mol2 files.
    """
    def __init__(self,input_file,verbose=True,delimiter="\t",name_field="_Name"):
        self.input_file = input_file
        self.verbose = True
        self.delimiter = delimiter
        file_name, file_extension = _splitext(input_file)
        self.file_name = file_name
        self.file_extension = file_extension
        if(file_extension not in ['.smi','.smiles','.sdf','.mol2']): 
            raise ValueError("Incorrect file extension")
        self.mols = []
        self.molserr = []
        self.nb_mols = None
        self.mols_ids = []
        self.name_field = name_field
        
    def ReadMolecules(self,titleLine=False,smilesColumn=0,nameColumn=1): #titleLine for smiles

        if self.file_extension in ['.smi','.smiles']:
            if self.verbose:
                print("Format of the structures file = SMILES")
            suppl = _SmilesMolSupplier(self.input_file,smilesColumn=smilesColumn,
                                           nameColumn=nameColumn,
                                           delimiter=self.delimiter,titleLine=titleLine)

            for i,m in enumerate(suppl):
                if m is not None:
                    self.mols.append(m)
                    mol_id = i if self.name_field == None else m.GetProp(self.name_field)
                    self.mols_ids.append(mol_id)
                else:
                    self.molserr.append(i)
            nb_mols=len(self.mols)
        elif self.file_extension == '.mol2':
            print("Format of the structures file = Mol2") 
            molss=[]
            with open(self.input_file) as fi:
                for mol2 in RetrieveMol2Block(fi):
                    rdkMolecule = _MolFromMol2Block(mol2)
                    molss.append(rdkMolecule)
            for i,m in enumerate(molss):
                if m is not None:
                    self.mols.append(m)
                    mol_id = i if self.name_field == None else m.GetProp(self.name_field)
                    self.mols_ids.append(mol_id)
                else:
                    self.molserr.append(i)
                    self.mols.append(m)
            self.nb_mols=len(self.mols)
        else:
            if self.verbose:
                print("Format of the structures file = SDF")
            suppl = _SDMolSupplier(self.input_file)
            for i,m in enumerate(suppl):
                if m is not None:
                    self.mols.append(m)
                    mol_id = i if self.name_field == None else m.GetProp(self.name_field)
                    self.mols_ids.append(mol_id)
                else:
                    self.molserr.append(i)
            self.nbMols=len(self.mols)
        
        if self.verbose:
            if len(self.molserr) !=0:
                print("%d molecules (starting at zero) could not be processed.\n"%(len(self.molserr)))
                err_file="incorrect_molecules.csv"
                print("This information has been saved in the following file: %s\n"%(err_file))
                #for x in self.molserr: print x
                print("NOTE: the indexes of the molecules start at zero. Thus the first molecule is molecule 0.")
                # Save the information about which molecules could not be processed correctly.
                _savetxt(err_file,self.molserr,fmt="%d")
            else:
                print("All molecules in the input file were processed correctly")

class GetDataSetInfo:
    '''
    Crate a dictionary: keys = substructure IDs, value = compound IDs.
        Thus, we know for a compound, which substructures it contains
    '''

    def __init__(self,name_field=None):
        self.name_field = name_field
        self.nb_substructures = None
        self.max_radius = None
        self.mols_ids = []
        self.substructure_dictionary = {}
    
    def _combine_dicts(self,a, b, op=_add):
        return dict(a.items() + b.items() + [(k, op(a[k], b[k])) for k in set(b) & set(a)])
        
    def extract_substructure_information(self,radii,mols):
        self.radii = radii
        global indexes_mols        
        for i,m in enumerate(mols):
               info={}
               fp = _GetMorganFingerprint(m,max(radii),bitInfo=info)
               mol_id = i if self.name_field == None else m.GetProp(self.name_field)
               self.mols_ids.append(mol_id)
               substructure_dictionary = {k:[mol_id] for k,v in info.iteritems() if v[0][1] in radii}
               self.substructure_dictionary = self._combine_dicts(substructure_dictionary,self.substructure_dictionary)
        self.nb_substructures = len(self.substructure_dictionary.keys())
        self.max_radius = max(radii)


# ### bioalerts: Alerts

# In[8]:


import numpy as np
import os
import sys
import rdkit
from rdkit import Chem
from rdkit.Chem import rdMolDescriptors
import rdkit.rdBase
from rdkit.Chem.MACCSkeys import GenMACCSKeys
from rdkit.Chem import AllChem
from rdkit import DataStructs
from rdkit.DataStructs import BitVectToText
import scipy as sc
import pandas as pd
from rdkit.Chem import PandasTools
from rdkit.Chem import Draw
from rdkit.Chem.Draw import IPythonConsole
from scipy import stats
from decimal import Decimal
import operator


class CalculatePvaluesCategorical:
    '''
    Calculate the p.values for each substructure in the input molecule
    n mols in the dataset
    m mols with a given activity label
    n' compounds with the substructure
    m' compounds from n' with the activity label
    '''
    def __init__(self,max_radius):
        # needs to be the same as the one used to calcualte the dictionary of substructures
        self.max_radius = max_radius  
        columns = ['Compound ID',
                   'Activity label',
                   'Substructure',
                   'Substructure in Molecule',
                   'p_value',
                   'Compounds with substr.',
                   'Comp. with substr. active',
                   'Comp. with substr. inactive',]
        self.output = pd.DataFrame(columns=columns)
        self.Bonferroni = None
    def calculate_p_values(self,mols,substructure_dictionary,bioactivities,mols_ids,threshold_frequency,
                           threshold_nb_substructures = 5,
                           threshold_pvalue = 0.05,
                           active_label=1,
                           inactive_label=0,
                           Bonferroni = True):
        self.Bonferroni = Bonferroni
        
        # n
        nb_mols = float(len(set([item for sublist in substructure_dictionary.values() for item in sublist])))
        # m
        nb_active_mols = float(np.sum(bioactivities == active_label))
        # (m - n)
        nb_inactive_mols = float(np.sum(bioactivities == inactive_label))

        nb_substructures_processed = 0
        if type(mols) != list: mols = [ext.mols[i] for i in np.arange(0,len(mols))]  #[x for x in mols]

        subs_discarded = [] # substructure that have been identified in other molecules. 
        for m,mol in enumerate(mols): #np.arange(0,len(mols)):
            #mol=mols[m]
            root_atoms_discarded = [] # center (or root) atoms discarded..
            info={}
            fp = AllChem.GetMorganFingerprint(mol,self.max_radius,bitInfo=info)
            # sort info to make sure the substructures are read from the smallest to the biggest.
            # In case a substructure with low radius is removed, we make sure all containing it will not be considered either in the following steps)
            # get keys sorted
            ff= sorted(info.iteritems(), key=operator.itemgetter(1))
            substructure_ids = [ff[x][0] for x in range(0,len(info))] 
            substructures_sub_dict = substructure_dictionary.keys()  

            for substructure_id in substructure_ids: 
                atom_radius = info[substructure_id]
                nb_substructures_processed += 1
                # check is the substructure is in the database (i.e. training data)
                if substructure_id in substructures_sub_dict and substructure_id not in subs_discarded and atom_radius[0][0] not in root_atoms_discarded:
                    mols_with_current_substructure = substructure_dictionary[substructure_id]
                    nb_comp_with_substructure = float(len(mols_with_current_substructure)) 
                    active_comp = (bioactivities == active_label)
                    comp_with_substructure = np.in1d(np.asarray(mols_ids) , np.asarray(mols_with_current_substructure))
                    nb_comp_with_substructure_active = np.sum(active_comp * comp_with_substructure) #i.e. m_{S act}
                    inactive_comp = (bioactivities == inactive_label)
                    #comp_with_substructure = np.in1d(np.asarray(mols_ids) , np.asarray(mols_with_current_substructure))
                    nb_comp_with_substructure_inactive = np.sum(inactive_comp * comp_with_substructure)

                    ## ACTIVE 
                    #########
                    #filter threshold of compounds with the substructure
                    filter_a = nb_comp_with_substructure > threshold_nb_substructures 
                    if filter_a: 
                        # filter threshold
                        filter_b = (float(nb_comp_with_substructure_active) / float(np.sum(comp_with_substructure))) > threshold_frequency
                        if filter_b:
                            p_value = 0 
                            for count in np.arange(nb_comp_with_substructure_active,nb_comp_with_substructure):
                                numerator = Decimal(sc.math.factorial(float(nb_comp_with_substructure)))
                                denominatorA = Decimal(sc.math.factorial(float(count))) * Decimal(sc.math.factorial(float(nb_comp_with_substructure-count)))
                                denominatorB = (nb_active_mols/nb_mols)**float(count)
                                denominatorC = (1.0 - (nb_active_mols/nb_mols))**(nb_comp_with_substructure - count)
                                out = float(numerator/denominatorA) * denominatorB * denominatorC
                                p_value += out 
                            
                            if p_value < threshold_pvalue:
                                #self.p_values_dictionary[substructure_id] = p_value
                        
                                # Drawing
                                env = Chem.FindAtomEnvironmentOfRadiusN(mol,atom_radius[0][1],atom_radius[0][0])
                                amap = {}
                                submol=Chem.PathToSubmol(mol,env,atomMap=amap)
                                m1 = mol
                                m1.GetSubstructMatch(submol)
                                #mm = Draw.MolToImage( mol,wedgeBonds=True,kekulize=True,highlightAtoms=amap.keys(),colour='green')
                                self.output = self.output.append({'Compound ID' : mols_ids[m],
                                                                  'Compounds with substr.' : nb_comp_with_substructure,
                                                                  #'Compounds with substr. and activity' : nb_comp_with_substructure_active,
                                                                  'p_value' : p_value,
                                                                  'Activity label':active_label,
                                                                  'Substructure in Molecule': m1,
                                                                  'Substructure':submol,
                                                                  'Comp. with substr. active':nb_comp_with_substructure_active,
                                                                  'Comp. with substr. inactive':nb_comp_with_substructure_inactive
                                                                  #'Smiles': Chem.MolToSmiles(mol)
                                                                  },ignore_index=True)
                                root_atoms_discarded.append(atom_radius[0][0])
                                subs_discarded.append(substructure_id)

                        
                        ## INACTIVE 
                        #########
                        #filter threshold of compounds with the substructure
                        # filter threshold
                        filter_b = (float(nb_comp_with_substructure_inactive) / float(np.sum(comp_with_substructure))) > threshold_frequency
                        if filter_b: 
                            p_value = 0 
                            for count in np.arange(nb_comp_with_substructure_inactive,nb_comp_with_substructure):
                                numerator = Decimal(sc.math.factorial(float(nb_comp_with_substructure)))
                                denominatorA = Decimal(sc.math.factorial(float(count))) * Decimal(sc.math.factorial(float(nb_comp_with_substructure-count)))
                                denominatorB = (nb_inactive_mols/nb_mols)**float(count)
                                denominatorC = (1.0 - (nb_inactive_mols/nb_mols))**(nb_comp_with_substructure - count)
                                out = float(numerator/denominatorA) * denominatorB * denominatorC
                                p_value += out 
                            
                            if p_value < threshold_pvalue:
                                #self.p_values_dictionary[substructure_id] = p_value
                        
                                # Drawing
                                env = Chem.FindAtomEnvironmentOfRadiusN(mol,atom_radius[0][1],atom_radius[0][0])
                                amap = {}
                                submol=Chem.PathToSubmol(mol,env,atomMap=amap)
                                m1 = mol
                                m1.GetSubstructMatch(submol)
                                #mm = Draw.MolToImage(mol,wedgeBonds=True,kekulize=True,highlightAtoms=amap.keys(),colour='red')
                                self.output = self.output.append({'Compound ID' : mols_ids[m],
                                                                  'Compounds with substr.' : nb_comp_with_substructure,
                                                                  #'Compounds with substr. and activity' : nb_comp_with_substructure_active,
                                                                  'p_value' : p_value,
                                                                  'Activity label':inactive_label,
                                                                  'Substructure in Molecule': m1,
                                                                  'Substructure':submol,
                                                                  'Comp. with substr. active':nb_comp_with_substructure_active,
                                                                  'Comp. with substr. inactive':nb_comp_with_substructure_inactive
                                                                  #'Smiles': Chem.MolToSmiles(mol)
                                                                  },ignore_index=True)
                                root_atoms_discarded.append(atom_radius[0][0])
                                subs_discarded.append(substructure_id)
                    else:
                        subs_discarded.append(substructure_id) 
                        root_atoms_discarded.append(atom_radius[0][0])
                                
        if self.Bonferroni == True:
           self.output['p_value'] = self.output['p_value'] * self.output.shape[0]
           self.output = self.output[self.output.p_value < 0.05]
        print('Number of substructures processed: ', nb_substructures_processed)
        print('Significant substructures: ', self.output.shape[0], 'substructures')

    def XlSXOutputWriter(self,output, output_filename, molCol=['Substructure',"Substructure in Molecule"], size=(300,300)):
        """
        http://nbviewer.ipython.org/github/Team-SKI/snippets/blob/master/IPython/rdkit_hackaton/XLSX%20export.ipynb
        Saves pandas DataFrame as a xlsx file with embedded images.
        It maps numpy data types to excel cell types:
        int, float -> number
        datetime -> datetime
        object -> string (limited to 32k character - xlsx limitations)
        
        Due to xlsxwriter limitations (other python xlsx writers have the same problem) 
        temporary image files have to be written to the hard drive.
        """
        
        import xlsxwriter 
           
        cols = list(output.columns)
        for i in molCol:
         cols.remove(i)

        dataTypes = dict(output.dtypes)
        
        workbook = xlsxwriter.Workbook(output_filename) # New workbook
        worksheet = workbook.add_worksheet() # New work sheet
        worksheet.set_column('A:A', size[0]/6.) # column width
        
        # Write first row with column names
        columns_nb = 0
        for x in molCol+cols:
            worksheet.write_string(0, columns_nb, x)
            columns_nb += 1
        
        row_nb = 1
        tmpfiles = []
        for index, row in output.iterrows():
            for j in np.arange(0,len(molCol)):
                imfile = "xlsx_tmp_img_%i_%i.png" %(row_nb,j)   ### HERE DRAW the substructures!!!!
                tmpfiles.append(imfile)
                worksheet.set_row(row_nb, height=size[1]) # looks like height is not in px?
                worksheet.set_column('A:B', size[0]/6.) # column width
                Draw.MolToFile(row[molCol[j]], imfile, size=size,kekulize=False)  # has to save a file on disk in order for xlsxwriter to incorporate the image
                worksheet.insert_image(row_nb, j, imfile)
        
            columns_nb = 2
            # Write data in columns and make some basic translation of numpy dtypes to excel data types
            for x in cols:
                if str(dataTypes[x]) == "object":
                    worksheet.write_string(row_nb, columns_nb, str(row[x])[:32000]) # string length is limited in xlsx
                elif ('float' in str(dataTypes[x])) or ('int' in str(dataTypes[x])):
                    if (row[x] != np.nan) or (row[x] != np.inf):
                        worksheet.write_number(row_nb, columns_nb, row[x])
                elif 'datetime' in str(dataTypes[x]):
                    worksheet.write_datetime(row_nb, columns_nb, row[x])
                columns_nb += 1
            row_nb += 1
        
        workbook.close()
        
        for f in tmpfiles:
            os.remove(f)



#############
class CalculatePvaluesContinuous:
    '''
    Calculate the p.values for each substructure in the input molecule
    '''
    def __init__(self,radii_ext):
        # needs to be the same as the one used to calcualte the dictionary of substructures
        self.radii_ext = radii_ext
        columns = ['Compound ID',
                   'Number compounds',
                   'statistic',
                   'p_value',
                   'Diff. distribution means (w - wo)',
                   'Compounds with substr.',
                   'Substructure',
                   'Substructure in Molecule'
                   ]
        self.output = pd.DataFrame(columns=columns)
        self.Bonferroni = None
    
    def calculate_p_values(self,
                           mols,
                           substructure_dictionary,
                           bioactivities,
                           mols_ids,
                           threshold_nb_substructures,
                           threshold_pvalue,
                           threshold_ratio,
                           Bonferroni=True
                           ):
        self.Bonferroni = Bonferroni
        nb_mols = float(len(set([item for sublist in substructure_dictionary.values() for item in sublist])))

        
        if type(mols) != list: mols = [mols[i] for i in np.arange(0,len(mols))]
        nb_substructures_processed=0
        already_processed = []
        for m,mol in enumerate(mols): 
            info={}
            fp = AllChem.GetMorganFingerprint(mol,max(self.radii_ext),bitInfo=info)
            substructures_sub_dict = substructure_dictionary.keys()
            
            for substructure_id, atom_radius in info.iteritems():
                nb_substructures_processed += 1
                # check if the substructure is in the reference dictionary
                if substructure_id in substructures_sub_dict and atom_radius[0][1] in self.radii_ext and substructure_id not in already_processed:
                    nb_comp_with_substructure = float(len(substructure_dictionary[substructure_id]))
                    
                    #filter threshold of compounds with the substructure
                    filter_a = nb_comp_with_substructure > threshold_nb_substructures
                    # filter threshold
                    filter_b = float(nb_comp_with_substructure / nb_mols) > threshold_ratio
                    if filter_a and filter_b:
                        mask = np.in1d(mols_ids,substructure_dictionary[substructure_id])
                        bio_substr = bioactivities[mask]
                        bio_wo_substr = bioactivities[np.logical_not(mask)]
                        # check normality
                        if sc.stats.shapiro(bio_substr) > 0.05 and sc.stats.shapiro(bio_wo_substr) > 0.05:
                            test = sc.stats.ttest_ind(bio_substr, bio_wo_substr)
                            p_value = test[1]
                            estatistic = test[0]
                        else:
                            test = sc.stats.ks_2samp(bio_substr, bio_wo_substr)
                            p_value = test[1]
                            estatistic = test[0]
                        if p_value < threshold_pvalue: 
                            env = Chem.FindAtomEnvironmentOfRadiusN(mol,atom_radius[0][1],atom_radius[0][0])
                            amap = {}
                            submol=Chem.PathToSubmol(mol,env,atomMap=amap)
                            m1 = mol
                            m1.GetSubstructMatch(submol)
                            already_processed.append(substructure_id)
                            self.output = self.output.append({
                                                             'Compound ID' : mols_ids[m],
                                                             'Number compounds':nb_mols,
                                                             'p_value': p_value,
                                                             'statistic': estatistic,
                                                             'Compounds with substr.': nb_comp_with_substructure,
                                                             'Substructure': submol,
                                                             'Substructure in Molecule': m1,
                                                             'Diff. distribution means (w - wo)': np.mean(bio_substr) - np.mean(bio_wo_substr),
                                                             },ignore_index=True)
           
        print('number of substructures processed: ', nb_substructures_processed)
        
        if self.Bonferroni == True:
            self.output['p_value'] = self.output['p_value'] * nb_substructures_processed
            self.output = self.output[self.output.p_value < 0.05]
        
    #def HTMLOutputWriter(self,output_filename):
    #    if os.path.exists(output_filename): os.remove(output_filename)
    #    with open(output_filename, 'w') as fo:
    #        fo.write(self.output.to_html())
            
    def XlSXOutputWriter(self,frame, output_filename, molCol=['Substructure',"Substructure in Molecule"], size=(300,300)):
        """
        http://nbviewer.ipython.org/github/Team-SKI/snippets/blob/master/IPython/rdkit_hackaton/XLSX%20export.ipynb
        Saves pandas DataFrame as a xlsx file with embedded images.
        It maps numpy data types to excel cell types:
        int, float -> number
        datetime -> datetime
        object -> string (limited to 32k character - xlsx limitations)
        
        Due to xlsxwriter limitations (other python xlsx writers have the same problem) 
        temporary image files have to be written to the hard drive.
        
        Cells with compound images are a bit larger than images due to excel.
        Column width weirdness explained (from xlsxwriter docs):
        The width corresponds to the column width value that is specified in Excel. 
        It is approximately equal to the length of a string in the default font of Calibri 11. 
        Unfortunately, there is no way to specify AutoFit for a column in the Excel file format.
        This feature is only available at runtime from within Excel.
        """
        
        import xlsxwriter # don't want to make this a RDKit dependency
           
        cols = list(frame.columns)
        for i in molCol:
         cols.remove(i)

        dataTypes = dict(frame.dtypes)
        
        workbook = xlsxwriter.Workbook(output_filename) # New workbook
        worksheet = workbook.add_worksheet() # New work sheet
        worksheet.set_column('A:A', size[0]/6.) # column width
        
        # Write first row with column names
        columns_nb = 0
        for x in molCol+cols:
            worksheet.write_string(0, columns_nb, x)
            columns_nb += 1
        
        row_nb = 1
        tmpfiles = []
        for index, row in frame.iterrows():
            for j in np.arange(0,len(molCol)):
                imfile = "xlsx_tmp_img_%i_%i.png" %(row_nb,j)   ### HERE DRAW the substructures!!!!
                tmpfiles.append(imfile)
                worksheet.set_row(row_nb, height=size[1]) # looks like height is not in px?
                worksheet.set_column('A:B', size[0]/6.) # column width
                Draw.MolToFile(row[molCol[j]], imfile, size=size,kekulize=False)  # has to save a file on disk in order for xlsxwriter to incorporate the image
                worksheet.insert_image(row_nb, j, imfile)
        
            columns_nb = 2
            # Write data in columns and make some basic translation of numpy dtypes to excel data types
            for x in cols:
                if str(dataTypes[x]) == "object":
                    worksheet.write_string(row_nb, columns_nb, str(row[x])[:32000]) # string length is limited in xlsx
                elif ('float' in str(dataTypes[x])) or ('int' in str(dataTypes[x])):
                    if (row[x] != np.nan) or (row[x] != np.inf):
                        worksheet.write_number(row_nb, columns_nb, row[x])
                elif 'datetime' in str(dataTypes[x]):
                    worksheet.write_datetime(row_nb, columns_nb, row[x])
                columns_nb += 1
            row_nb += 1
        
        workbook.close()
        
        for f in tmpfiles:
            os.remove(f)
            



# ### bioalerts: FPCalculator

# In[9]:


from numpy import arange as _arange, asarray as  _asarray, array as _array, sort as _sort, ones as _ones, zeros as  _zeros
from rdkit.Chem import SmilesMolSupplier as _SmilesMolSupplier, MolFromMol2Block as _MolFromMol2Block, SDMolSupplier as _SDMolSupplier, MolToSmiles as _MolToSmiles, PathToSubmol as _PathToSubmol, FindAtomEnvironmentOfRadiusN as _FindAtomEnvironmentOfRadiusN
from operator import add as _add
from rdkit.Chem.AllChem import GetMorganFingerprintAsBitVect as  _GetMorganFingerprintAsBitVect, GetMorganFingerprint as _GetMorganFingerprint
from os.path import splitext as _splitext, exists as _exists 
from os import makedirs as _makedirs
from rdkit.Chem.Draw import MolToFile as _MolToFile

class CalculateFPs:
    '''
    Calculate fps for a set of molecules
    Required input: substructure dictionary, radii to be considered
    '''
    def __init__(self,radii,mols,reference_substructure_keys={}):
        self.radii = radii
        self.max_radius = max(radii)
        if type(mols) != list: mols = [ext.mols[i] for i in _arange(0,len(mols))] 
        self.mols = mols
        self.reference_substructure_keys = reference_substructure_keys
        self.substructure_dictionary = {}
        self.mols_reference_for_unhashed = None
        self.columns_unhashed = None 
        self.substructure_ids = None
        # output
        self.fps_hashed_binary_quick = None
        self.fps_hashed_binary = None
        self.fps_hashed_counts = None
        self.fps_unhashed_binary = None
        self.fps_unhashed_counts = None
        self.substructures_smiles = {}
        
    def _combine_dicts(self,a, b, op=_add):
        return dict(a.items() + b.items() + [(k, op(a[k], b[k])) for k in set(b) & set(a)])
    
    def calculate_hashed_fps_binary_quick(self,nBits):
        # bit format
        self.fps_hashed_binary_quick = _asarray([_GetMorganFingerprintAsBitVect(x,radius=self.max_radius,nBits=nBits) for x in self.mols])
            
    def calculate_hashed_fps(self,nBits):
        # count format
        fps_hashed_binary = _zeros((len(self.mols),nBits), dtype=int)
        fps_hashed_counts = _zeros((len(self.mols),nBits), dtype=int)
        for mol_index,mol in enumerate(self.mols): 
            info={}
            fp = _GetMorganFingerprint(mol,radius=self.max_radius,bitInfo=info)
            for key,val in info.iteritems():
                if val[0][1] in self.radii: #check if the radius is in the selection
                    fps_hashed_binary[mol_index,key%nBits] = 1
                    fps_hashed_counts[mol_index,key%nBits] += len(val)
        self.fps_hashed_binary = fps_hashed_binary
        self.fps_hashed_counts = fps_hashed_counts
                    
    
    def calculate_unhashed_fps(self,draw_substructures=False,image_directory='./images_substructures'): 
        # get the dictionary for the substructures
        idxs = []
        substr_ids = []
        counts = []    
        for mol_index,mol in enumerate(self.mols):
            info={}
            fp = _GetMorganFingerprint(mol,radius=self.max_radius,bitInfo=info)
            substructure_dictionary = {k:[mol_index] for k,v in info.iteritems() if v[0][1] in self.radii}
            substr_ids.append(substructure_dictionary.keys())
            idxs.append([mol_index]*len(substructure_dictionary.keys()))
            counts.append([ len(info.values()[x]) for x in _arange(0,len(info)) if info.values()[x][0][1] in self.radii])
            
            # get the smiles for the substructures
            amap = {}
            substructures_smiles = {k:[_MolToSmiles(_PathToSubmol(mol,_FindAtomEnvironmentOfRadiusN(mol,v[0][1],v[0][0]),atomMap=amap))] for k,v in info.iteritems() if v[0][1] in self.radii}
            self.substructures_smiles.update(substructures_smiles)
            
            # generate the images for the substructures if required..
            if draw_substructures:
                if not _exists(image_directory):
                    _makedirs(image_directory)
                for k,v in info.iteritems():
                    if k not in self.substructure_dictionary.keys() and v[0][1] in self.radii:
                        image_name="%s/Molecule_%d_substr_%d.pdf"%(image_directory,mol_index,k)
                        env=_FindAtomEnvironmentOfRadiusN(mol,v[0][1],v[0][0])
                        amap={}
                        submol=_PathToSubmol(mol,env,atomMap=amap)
                        _MolToFile(mol,image_name,size=(300,300),wedgeBonds=True,kekulize=True,highlightAtoms=amap.keys())
            
            self.substructure_dictionary = self._combine_dicts(substructure_dictionary,self.substructure_dictionary)
      
            
        idxs = _array([val for sublist in idxs for val in sublist])
        counts = _array([val for sublist in counts for val in sublist])
        substr_ids_flattened = [val for sublist in substr_ids for val in sublist]
        substr_ids = _array(substr_ids_flattened)
        self.substructure_ids = substr_ids
        if len(self.reference_substructure_keys)==0:
            print("No input set of keys for the substructures. \nThus, the substructures present in the input molecules will be considered for the calculation of unhashed fingerprints.")
            columns = _array(list(set(self.substructure_dictionary.keys())))
            columns = _sort(columns)
            self.columns_unhashed = columns
            dimensionality_unhashed = len(columns)
        else:
            columns = _array(list(set(self.reference_substructure_keys)))
            columns = _sort(columns)
            self.columns_unhashed = columns
            dimensionality_unhashed = len(columns)
        
        fps_unhashed_binary = _zeros((len(self.mols),dimensionality_unhashed), dtype=int)
        fps_unhashed_counts = _zeros((len(self.mols),dimensionality_unhashed), dtype=int)
        

        # removing the indices corresponding to the substructures in the test molecules not present in the references set of substructures..
        idxs = _array([idxs[x] for x in _arange(0,len(substr_ids)) if substr_ids[x] in self.columns_unhashed])    
        counts = _array([counts[x] for x in _arange(0,len(substr_ids)) if substr_ids[x] in self.columns_unhashed]) 
        substr_ids = _array([substr_ids[x] for x in _arange(0,len(substr_ids)) if substr_ids[x] in self.columns_unhashed])
        mapping = _array([(substr_ids[x]==columns).nonzero() for x in _arange(0,len(substr_ids))])
        mapping = mapping.flatten()
        if len(mapping) ==0:
            print("There is no intersection between the substructures \n(i)provided in the reference key set, and\n(ii) the substructures found in the input molecules.")
            return

        fps_unhashed_binary[idxs,mapping] = _ones(len(counts))
        fps_unhashed_counts[idxs,mapping] = counts
        self.fps_unhashed_binary = fps_unhashed_binary
        self.fps_unhashed_counts = fps_unhashed_counts


# In[10]:


from rdkit.Chem.Draw import IPythonConsole
from rdkit.Chem import PandasTools
# Machine learning modules
import sklearn
from sklearn import preprocessing
import scipy
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score


# In[11]:


import sys, numpy as np, scipy as sc, rdkit, matplotlib as pylab, pandas as pd, IPython
print(" Python:", sys.version, "\n")
print(" Numpy:", np.__version__) 
print(" Scipy:", sc.__version__) 
print(" Rdkit:", rdkit.rdBase.rdkitVersion) 
print(" Matplotlib:", pylab.__version__) 
print(" Pandas:", pd.__version__) 
print(" Ipython:", IPython.__version__) 
print(" Scikit-Learn:", sklearn.__version__) 
print(" Scipy:", scipy.__version__) 


molecules = LoadMolecules("data/Young_2002.sdf", verbose=False)
molecules.ReadMolecules()
print("Total number of input molecules: ", len(molecules.mols))

stride = int(len(molecules.mols) * 0.9)
training = molecules.mols[0:stride]
test = molecules.mols[stride:len(molecules.mols)]
print(len(molecules.mols), len(training), len(test))

molecules.mols_ids[0:5]

training_dataset_info = GetDataSetInfo()

training_dataset_info.extract_substructure_information(radii=[2,3,4],
                                                       mols=training)
