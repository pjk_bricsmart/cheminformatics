# -*- coding: utf-8 -*-
"""Created on Fri Feb 25 12:10 2019
@author: Paul J Kowalczyk
Toxicology
"""

### Instantiate environment
from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import PandasTools
import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt 
import numpy as np
import math
from sklearn.ensemble import RandomForestRegressor
import matplotlib.pyplot as plt
import datetime

### Read data
df = PandasTools.LoadSDF("data/hERG_PubChem.sdf")

df = df[['ID', 'Outcome', 'ROMol']]

for i in range(len(df)):
    Chem.rdmolops.Cleanup(df.iloc[i, 2])
    Chem.SanitizeMol(df.iloc[i, 2])

for i in range(len(df)):
    df.at[i, 'SMILES'] = Chem.MolToSmiles(df.iloc[i, 2])
        
### Calculate features
nms = [x[0] for x in Descriptors._descList]
calc = MoleculeDescriptors.MolecularDescriptorCalculator(nms)
for i in range(len(df)):
    descrs = calc.CalcDescriptors(df.iloc[i, 2])
    for x in range(len(descrs)):
        df.at[i, str(nms[x])] = descrs[x]
        
df = df.replace([np.inf, -np.inf], np.nan)

df = df.dropna()

df.shape

print(df['Outcome'].unique())
print(df.groupby('Outcome').size())

import seaborn as sns
sns.countplot(df['Outcome'], label = 'Count')
plt.show()

for i in range(len(df)):
    if df.iloc[i, 1] == 'Active':
        df.at[i, 'Active'] = 1
    else:
        df.at[i, 'Active'] = 0

print(df['Active'].unique())
print(df.groupby('Active').size())

import seaborn as sns
sns.countplot(df['Active'], label = 'Count')
plt.show()    


# df = df.loc[:,~df.columns.str.startswith('fr')]

### Training & Test Datasets
X = df.drop(columns=["ID", "Outcome", "ROMol", "Active"])
y = df[["Active"]]
X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    random_state = 350,
                                                    test_size = 0.2)

### Identify / remove near-zero variance descriptors
def variance_threshold_selector(data, threshold = 0.5):
    selector = VarianceThreshold(threshold)
    selector.fit(data)
    return data[data.columns[selector.get_support(indices = True)]]

nzv = variance_threshold_selector(X_train, 0.0)

X_train = X_train[nzv.columns]
X_test = X_test[nzv.columns]
   
### Identify / remove highly correlated descriptors
corr_matrix = X_train.corr().abs()
upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),
                                  k = 1).astype(np.bool))
to_drop = [column for column in upper.columns
           if any(upper[column] > 0.85)]

X_train = X_train[X_train.columns.drop(to_drop)]
X_test = X_test[X_test.columns.drop(to_drop)]

### standardize features by removing the mean and scaling to unit variance
scaler = StandardScaler()
scaler.fit(X_train)

X_train_standard = scaler.transform(X_train)
X_test_standard = scaler.transform(X_test)

print(datetime.datetime.now())

from tpot import TPOTClassifier
tpot = TPOTClassifier(generations=10, population_size=50, verbosity=2)
tpot.fit(X_train_standard, y_train)
print(tpot.score(X_test_standard, y_test))
tpot.export('tpot_hERG.py')

print(datetime.datetime.now())
