# -*- coding: utf-8 -*-
"""
Created on Sat Feb 23 09:32:10 2019

@author: us16120
"""

import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_validate
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, recall_score, precision_score
from sklearn.metrics import classification_report,confusion_matrix
from imblearn.over_sampling import SMOTE
from sklearn.model_selection import GridSearchCV
import pickle

#####
##### unpickle training and test sets
#####
filename = 'train_X'
infile = open(filename, 'rb')
X_train = pickle.load(infile)
infile.close()
filename = 'test_X'
infile = open(filename, 'rb')
X_test = pickle.load(infile)
infile.close()
filename = 'train_y'
infile = open(filename, 'rb')
y_train = pickle.load(infile)
infile.close()
filename = 'test_y'
infile = open(filename, 'rb')
y_test = pickle.load(infile)
infile.close()

sm = SMOTE(random_state=12, ratio = 0.7)

X_train_res, y_train_res = sm.fit_sample(X_train, y_train)
cls = KNeighborsClassifier()

K = list(range(5,15))

param_grid = {'n_neighbors': K}

scoring = ['accuracy', 'recall', 'precision']

gs = GridSearchCV(estimator=cls,
 param_grid=param_grid,
 scoring='recall',
 n_jobs=-1,
 cv=10)

gs_fit = gs.fit(X_train_res, y_train_res)
print('Best parameters %s' % gs_fit.best_params_)

cls_best = KNeighborsClassifier(n_neighbors=5)

scores = cross_validate(cls_best, X_train_res, y_train_res, scoring=scoring, cv=10)

print('\nAverage Accuracy %.2f +/- %.2f' % (np.mean(scores['test_accuracy']),
                                            np.std(scores['test_accuracy'])))
print('\nAverage Recall %.2f +/- %.2f' % (np.mean(scores['test_recall']),
                                          np.std(scores['test_recall'])))
print('\nAverage Precision %.2f +/- %.2f' % (np.mean(scores['test_precision']),
                                             np.std(scores['test_precision'])))

model = cls_best.fit(X_train_res, y_train_res)

train_acc = accuracy_score(y_true=y_train_res, y_pred=model.predict(X_train_res))
train_rec = recall_score(y_true=y_train_res, y_pred=model.predict(X_train_res))
train_prec = precision_score(y_true=y_train_res, y_pred=model.predict(X_train_res))
test_acc = accuracy_score(y_true=y_test, y_pred=model.predict(X_test))
test_rec = recall_score(y_true=y_test, y_pred=model.predict(X_test))
test_prec = precision_score(y_true=y_test, y_pred=model.predict(X_test))
predictions = model.predict(X_test)
print('Training accuracy: %.2f' % train_acc)
print('Training recall: %.2f' % train_rec)
print('Training precision: %.2f' % train_prec)
print('Test accuracy: %.2f' % test_acc)
print('Test recall: %.2f' % test_rec)
print('Test precision: %.2f' % test_prec)
print(classification_report(y_test,predictions))
print(confusion_matrix(y_test,predictions))