# -*- coding: utf-8 -*-
"""
Created on Sun Feb 24 20:47:20 2019

@author: us16120
"""

import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.model_selection import cross_validate
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, recall_score, precision_score
from sklearn.metrics import classification_report,confusion_matrix
from imblearn.over_sampling import SMOTE
import pickle

#####
##### unpickle training and test sets
#####
filename = 'train_X'
infile = open(filename, 'rb')
X_train = pickle.load(infile)
infile.close()
filename = 'test_X'
infile = open(filename, 'rb')
X_test = pickle.load(infile)
infile.close()
filename = 'train_y'
infile = open(filename, 'rb')
y_train = pickle.load(infile)
infile.close()
filename = 'test_y'
infile = open(filename, 'rb')
y_test = pickle.load(infile)
infile.close()

from sklearn.model_selection import GridSearchCV

sm = SMOTE(random_state=12, ratio = 0.7)
X_train_res, y_train_res = sm.fit_sample(X_train, y_train)

cls = LinearSVC(random_state=350)

param_grid = {'C': [1, 10, 100]}
scoring = ['accuracy', 'recall', 'precision']

gs = GridSearchCV(estimator=cls,
                  param_grid=param_grid,
                  scoring='recall',
                  n_jobs=-1,
                  cv=10)

gs_fit = gs.fit(X_train_res, y_train_res)
print('Best parameters %s' % gs_fit.best_params_)

cls_best = LinearSVC(C=1, random_state= 350)

scores = cross_validate(cls_best, X_train_res, y_train_res, scoring=scoring, cv=10)
