# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 15:18:04 2019

@author: P J Kowalczyk
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# %matplotlib inline
import seaborn as sns
from pubchempy import get_compounds, Compound
from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import PandasTools
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import train_test_split
import datetime

df = pd.read_csv('data\AID_588834_datatable_all_CURATED.csv').dropna()

df.reset_index(drop = True, inplace = True)

df = df.astype({"PUBCHEM_SID": 'int64',
                "PUBCHEM_CID": 'int64',
                "PUBCHEM_ACTIVITY_OUTCOME": str})

df.info()

print(df['PUBCHEM_ACTIVITY_OUTCOME'].unique())
print(df.groupby('PUBCHEM_ACTIVITY_OUTCOME').size())

sns.countplot(df['PUBCHEM_ACTIVITY_OUTCOME'], label = 'Count')
plt.show()

for i in range(len(df)):
# for i in range(10):
    try:
#        comp = Compound.from_cid((df.iloc[i,1],))
        df.at[i, 'Canonical_SMILES'] = \
        Compound.from_cid((df.iloc[i, 1],)).canonical_smiles
    except:
        df.at[i, 'Canonical_SMILES'] = 'NaN'
            
df.dropna(inplace = True)

df.to_csv('data\AID_588834_SMILES.csv')

#####
# KNIME: Structure_Curation
#####

df = pd.read_csv('data\AID_588834_SMILES_KNIME_curated.csv').dropna()

for i in range(len(df)):
    try:
        m = Chem.MolFromSmiles(df.iloc[i, 1])
        df.at[i, 'Molecule'] = m
    except:
        df.at[i, 'Molecule'] = 'NaN'
        
df.dropna(inplace = True)

### Calculate features
nms = [x[0] for x in Descriptors._descList] # 200 descriptors
calc = MoleculeDescriptors.MolecularDescriptorCalculator(nms)
for i in range(len(df)):
    try:
        descrs = calc.CalcDescriptors(df.iloc[i, 3])
        for x in range(len(descrs)):
            df.at[i, str(nms[x])] = descrs[x]
    except:
        for x in range(len(descrs)):
            df.at[i, str(nms[x])] = 'NaN'  
            
df = df.replace([np.inf, -np.inf], np.nan)
           
df.dropna(inplace = True)

df.reset_index(drop = True, inplace = True)

for i in range(len(df)):
    if df.iloc[i, 0] == 'Active':
        df.at[i, 'Active'] = 1
    else:
        df.at[i, 'Active'] = 0

print(df['PUBCHEM_ACTIVITY_OUTCOME'].unique())
print(df.groupby('PUBCHEM_ACTIVITY_OUTCOME').size())

sns.countplot(df['PUBCHEM_ACTIVITY_OUTCOME'], label = 'Count')
plt.show()

### Training & Test Datasets
X = df.drop(columns=["PUBCHEM_ACTIVITY_OUTCOME",
                      "SMILES", "InChI", "Molecule", "Active"])
y = df[["Active"]]
X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    random_state = 350,
                                                    test_size = 0.2)

### Identify / remove near-zero variance descriptors
def variance_threshold_selector(data, threshold = 0.5):
    selector = VarianceThreshold(threshold)
    selector.fit(data)
    return data[data.columns[selector.get_support(indices = True)]]

nzv = variance_threshold_selector(X_train, 0.0)

X_train = X_train[nzv.columns]
X_test = X_test[nzv.columns]

### Identify / remove highly correlated descriptors
corr_matrix = X_train.corr().abs()
upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),
                                  k = 1).astype(np.bool))
to_drop = [column for column in upper.columns
           if any(upper[column] > 0.85)]

X_train = X_train[X_train.columns.drop(to_drop)]
X_test = X_test[X_test.columns.drop(to_drop)]

### standardize features by removing the mean and scaling to unit variance
scaler = StandardScaler().fit(X_train)

X_train_standard = scaler.transform(X_train)
X_test_standard = scaler.transform(X_test)

#####
##### TPOT
#####

print(datetime.datetime.now())
from tpot import TPOTClassifier
tpot = TPOTClassifier(generations=10, population_size=50, verbosity=2)
tpot.fit(X_train_standard, y_train)
print(tpot.score(X_test_standard, y_test))
tpot.export('tpot_PubChem_588834_pipeline.py')
print(datetime.datetime.now())
