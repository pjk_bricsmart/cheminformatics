import pandas as pd
import numpy as np
import os
from matplotlib import pyplot as plt

PATH_DATASET = 'C:/Tristan/IA/HSPIP/Output/output_HSP_full_test.csv'
SEP=','

csv_path = os.path.join(PATH_DATASET)
dataset = pd.read_csv(csv_path,sep=SEP)
shift = 0
y = np.asarray(dataset.iloc[:,0:3])
prediction = np.asarray(dataset.iloc[:,3:6])

print(y.shape,dataset)

for idx in range(y.shape[1]):
    ax = plt.subplot(np.int(np.ceil(y.shape[1]/2)),2,idx+1)
    #ax.set_title(d_test.keys()[idx+1])
    err = np.square(prediction[:,idx]-y[:,idx])
    mse = np.mean(err, axis=0)
    var = np.var(y[:,idx])
    #ax.legend(loc='best')

    r_square = 1-(mse/var)
    ax.plot(y[:,idx], prediction[:,idx],'b*')
    ax.plot([np.min(y[:,idx]),np.max(y[:,idx])],[np.min(y[:,idx]),np.max(y[:,idx])],'r')


    ax.legend([str(dataset.keys()[idx+shift]),str(round(r_square,3))])
    plt.xlabel(str(dataset.keys()[idx+shift]))
    plt.ylabel('pred ' + str(dataset.keys()[idx+shift]))
    #plt.legend(r_square)
    print("R-squared  -->  ",r_square)

plt.show()