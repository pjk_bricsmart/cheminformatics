#####
#
# C:\Users\us16120\Projects\Cheminformatics\FlashPoint.py
#
#####

from rdkit import Chem
from rdkit.Chem import rdmolops
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import AllChem
from rdkit import DataStructs
from rdkit.Chem import MACCSkeys
from rdkit.Chem import PandasTools
from rdkit.Chem.AtomPairs import Pairs, Torsions
from rdkit.Chem import rdMolDescriptors
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVR
from sklearn.feature_selection import VarianceThreshold
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import RandomizedSearchCV
from sklearn import metrics
from sklearn.metrics import r2_score
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression
from sklearn.feature_selection import RFECV
from scipy import stats
import cirpy
import pandas as pd
import pickle
import re
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
# %matplotlib

# Data acquisition

df = pd.read_csv('FlashPoint.csv')
df['FP'] = df['FP.Exp.']
df = df[['Name', 'Family', 'SMILES', 'FP']]

for i in range(len(df)):
    try:
        mol = Chem.MolFromSmiles(df.loc[i, 'SMILES'])
        df.loc[i, 'Molecule'] = mol
    except:
        df.loc[i, 'Molecule'] = 'NaN'   

df = df.dropna()

for i in range(len(df)):
    df.loc[i, 'InChI'] = Chem.inchi.MolToInchi(df.loc[i, 'Molecule'])
    
for i in range(len(df)):
    df.loc[i, 'NoStereo'] = df.loc[i, 'SMILES'].replace("@@", "").replace("@", "")
    df.loc[i, 'NoStereo'] = df.loc[i, 'NoStereo'].replace("/", "").replace("\\", "")

for i in range(len(df)):
    try:
        mol = Chem.MolFromSmiles(df.loc[i, 'NoStereo'])
        df.loc[i, 'NoStereoSMILES'] = Chem.MolToSmiles(mol)
        df.loc[i, 'NoStereoMolecule'] = mol
    except:
        df.loc[i, 'NoStereoSMILES'] = 'NaN'
        df.loc[i, 'Molecule'] = 'NaN'   
        
df.to_csv('FlashPointStereo.csv')
