NAME	SMILES
pseudosydnonimine	C[N+]1=NOCC1
3-morpholinosydnonimine	[Cl-].Nc1c[n+](no1)N2CCOCC2
chloromethylidene-drv	[Cl-].C/[N+](C)=C\Cl
bromomethylidene-drv	[Cl-].C/[N+](C)=C\Br
iodomethylidene-drv	[Cl-].C/[N+](C)=C\I
fluoromethylidene-drv	[Cl-].C/[N+](C)=C\F
Pemirolast potassium	[K+].CC3=CC=CN1C3=NC=C(C1=O)c2[n-]nnn2
Pemirolast	CC3=CC=CN1C3=NC=C(C1=O)c2[nH]nnn2
Pemirolast-alt	CC3=CC=CN1C3=NC=C(C1=O)c2nnnn2
FR130739	[Na+].CCCCc5nc1cccnc1n5Cc2ccc(cc2)c4ccccc4c3[n-]nnn3
N-Dodecyl-N,N-dimethyl-3-ammonio-1-propanesulfonate	[O-]S(=O)(=O)CCC[N+](C)(C)CCCCCCCCCCCC
ND-copy	OS(=O)(=O)CCC[N+](C)(C)CCCCCCCCCCCC
Hydroxymethanesulfinic acid monosodium salt dihydrate	O.O.[Na+].OCS(=O)(=O)O
Hydroxymethanesulfinic acid monosodium salt dihydrate	O.O.[Na]OCS(=O)O
(2,2,6,6-Tetramethyl-4-oxopiperidin-1-yl)oxidanyl	[O]N(C(C)(C)C1)C(C)(C)CC1=O
4-Hydroxy-2,2,6,6-tetramethylpiperidin-1-yl)oxidanyl	OC1CC(C)(C)N([O])C(C)(C)C1
hydroxyl_amine	ON(C(C)(C)C1)C(C)(C)CC1=O
hydroxyl-_amine	[O-]N(C(C)(C)C1)C(C)(C)CC1=O
Co-stooff	[Co+2].[O-]CCC.[O-]CCC
Ag-stoff	[Ag+].[O-]CCC
Ag-ho	[Ag].CCCCO
