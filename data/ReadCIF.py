# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 22:08:34 2020

@author: US16120
"""


import tarfile

t = tarfile.open('Polymer-CIF.tgz', 'r')
print(t.getnames())
t.extractall('polymer')

from gemmi import cif

# read and parse a CIF file
doc = cif.read_file('polymer/Polymer-CIF/0502.cif')

block = doc.sole_block()

block.name
